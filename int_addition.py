''' Program to find numbers in a string and add them'''

string = "200 plus 500 is equal to"



def no_int(string):
    # splitting string word by word
    string_list = string.split()

    digit_ist = []
    number_of_integers = 0

    for item in string_list:
        if item.isdigit():
            number_of_integers += 1
            item = int(item)
            digit_ist.append(item)

    sum_ = sum(digit_ist)
    print(f"Given string is : {string}")
    print(str(number_of_integers) + " integers found")
    print("sum: " + str(sum_))
    return sum_


no_int(string)