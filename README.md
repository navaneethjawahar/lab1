# lab1

Creating a project and Repo in Git lab for lab 1 exercise

## Name
AIDI-2004: Artificial Intelligence in Enterprise Systems  
Lab #1 – Git, GitHub, GitLab 


## Description
Lab assignment for GIT practice. 

This assignment relates to the following Course Learning Outcomes: 
 
CLO-2: Write industry standard code and use git to work in agile environment for developing enterprise AI products 
CLO-6: Develop and deploy Enterprise AI solution pipeline using various technologies (i.e., Local, 
Dockers, Heroku or cloud) 
Lab #1 – Git 

## Program description:
Program to find numbers in a string and add them - 

- Splitting the given string with a space 
- checking if the token is integer 
- Adding and storing the integer count to an index
- returning the sum of stored integer




## Authors and acknowledgment
- Author: Navaneeth Jawahar 
- Program Idea: Intermediate Coding challenges from Geeks for Geeks


